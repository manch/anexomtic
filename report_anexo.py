from docx import Document
from docx.shared import Inches, Cm, Pt
from docx.enum.table import WD_TABLE_ALIGNMENT



def addTicket(document=None, titles=[], widths=[],data={}):
    titles = ['CICLO','SEMANA','PLATAFORMA','DÍA','HORA','CURSO','GRUPO','NOMBRE','CC','TIEMP.','ASISTIÓ']
    widths = [1.28, 1.59, 2.25, 1.77, 1.48, 1.25, 1.32, 3, 2, 1.25, 1.43]
    '''
    data= {'CICLO': 3, 'SEMANA': 1, 'PLAT': 'Correo ', 'DÍA': '1/09/2021', 'HORA': '12:00 p.\xa0m.', 'CURSO': 'DS', 'GRUPO': 'S3', 'NOMBRE': 'Yira Avila', 'DOCID': 59666633, 'TIME': 74, 'ASIST': 'SI', 'numTicket': 114509, 'linkTicket': 'https://mda.uis.edu.co/scp/tickets.php?id=3319'}
    '''
    numId = document.get_new_list("10")
    oneText=document.add_paragraph(
        'Resumen de Tutoria: Ticket#'+str(data['numTicket']), style='ListParagraph')
    oneText.num_id=numId

    table = document.add_table(rows=2, cols=11)
    table.autofit=False
    table.alignment = WD_TABLE_ALIGNMENT.CENTER
    table.style = 'TableGrid'

    hdr_cells = table.rows[0].cells
    table.rows[0].height = Cm(0.7)
    table.rows[1].height = Cm(0.7)

    for i,dt in enumerate(titles):
        hdr_cells[i].text = dt
        hdr_cells[i].paragraphs[0].runs[0].font.size = Pt(8)
        hdr_cells[i].width = Cm(widths[i])


    row_cells = table.rows[1].cells
    #print(data.values())
    #exit
    # print(list(data.values())[:-2])

    for id,dt in enumerate(list(data.values())[:-3]):
        #print(id,dt)
        row_cells[id].text = str(dt)
        row_cells[id].paragraphs[0].runs[0].font.size = Pt(8)
        row_cells[id].width = Cm(widths[id])

    secondText=document.add_paragraph(
        'Impresion de Pantalla MDA', style='ListParagraph'
    )
    secondText.num_id=numId

    document.add_picture('.//pics//'+str(data['numTicket'])+'.png', width=Cm(16))

    thirdText=document.add_paragraph(
        'Link de Grabacion', style='ListParagraph'
    )
    thirdText.num_id=numId
    if(data['linkZoom']==""):
        document.add_paragraph('https://mailuis-my.sharepoint.com/:f:/g/personal/misiontic_tutor4_uis_edu_co/EtGrnxvQOfJDqIkGwA8r0a0B36qqz619olwC7uFFILnNwQ?e=imzuhf')
    else:
        document.add_paragraph(data['linkZoom'])
    document.add_paragraph('\n')
    
if __name__=="__main__":
    document = Document()
    addTicket(document=document)
    addTicket(document=document)
    addTicket(document=document)
    document.save('demo.docx')