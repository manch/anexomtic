from docx import Document

document = Document()

# Add desired numbering styles to your template file.
# Extract abstractNumId from there. In this example, abstractNumId is 10
numId = document.get_new_list("10")

# Add a list
p = document.add_paragraph(style = 'ListParagraph', text = "a")
p.num_id = numId
p.level = 0
p = document.add_paragraph(style = 'ListParagraph', text = "b")
p.num_id = numId
p.level = 1
p = document.add_paragraph(style = 'ListParagraph', text = "c")
p.num_id = numId
p.level = 1
p = document.add_paragraph(style = 'ListParagraph', text = "d")
p.num_id = numId
p.level = 0
p = document.add_paragraph(style = 'ListParagraph', text = "e")
p.num_id = numId
p.level = 1
p = document.add_paragraph(style = 'ListParagraph', text = "f")
p.num_id = numId
p.level = 0

# Restart numbering at the outer level
numId = document.get_new_list("10")

# Add the same list once again. The numbering is restarted
p = document.add_paragraph(style = 'ListParagraph', text = "a")
p.num_id = numId
p.level = 0
p = document.add_paragraph(style = 'ListParagraph', text = "b")
p.num_id = numId
p.level = 1
p = document.add_paragraph(style = 'ListParagraph', text = "c")
p.num_id = numId
p.level = 1
p = document.add_paragraph(style = 'ListParagraph', text = "d")
p.num_id = numId
p.level = 0
p = document.add_paragraph(style = 'ListParagraph', text = "e")
p.num_id = numId
p.level = 1
p = document.add_paragraph(style = 'ListParagraph', text = "f")
p.num_id = numId
p.level = 0

document.save("num.docx")