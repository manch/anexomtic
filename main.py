import os
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys

from PIL import Image
from Screenshot import Screenshot_Clipping
from report_anexo import *

import time, xlrd, datetime



driver_path=r'..//..//..//Drivers//chromedriver.exe'

def open_browser(driver_path=driver_path):
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument("--disable-infobars")
    chrome_options.add_argument("--disable-logging")
    chrome_options.add_argument("--disable-login-animations")
    chrome_options.add_argument("--disable-notifications")
    chrome_options.add_argument("--disable-default-apps")
    chrome_options.add_argument("--log-level=3")
    return webdriver.Chrome(options=chrome_options,executable_path = driver_path )

def openMDA(browser=None, data_login={}):
    urlMDA='https://mda.uis.edu.co/scp/login.php'
    browser.get(urlMDA)
    time.sleep(1)
    browser.find_element_by_xpath('//input[@name="userid"]').send_keys(data_login['user'])
    #browser.find_element_by_xpath('//input[@name="userid"]').send_keys('miguelnoriega')
    browser.find_element_by_xpath('//input[@name="passwd"]').send_keys(data_login['psswd'])
    #browser.find_element_by_xpath('//input[@name="passwd"]').send_keys('*******')
    browser.find_element_by_xpath('//button[@name="submit"]').click()
    pass

def takepic(browser=None, linkpic="", numTicket=0000, debug=False):
    browser.get(linkpic)
    time.sleep(1)
    browser.execute_script("window.scrollTo(0, 0)")
    #ss = Screenshot_Clipping.Screenshot()
    #image = ss.full_Screenshot(browser, save_path=r'.//pics' , image_name=str(numTicket)+'.png')
    browser.save_screenshot('.//pics//'+str(numTicket)+'.png')
    if debug:
        screen = Image.open('.//pics//'+str(numTicket)+'.png')
        screen.show()

def return_value_xls(sheet,j,colum_titles_num,string):
	return_value=sheet.cell_value(j, colum_titles_num[string])
	if type(return_value)==str:
		return return_value
	else:
		return int(return_value)

def read_data(file_excel=''):
    loc = (file_excel)   
    wb = xlrd.open_workbook(loc) 
    sheet = wb.sheet_by_index(0)
    colum_titles=['CICLO','SEMANA','PLAT','DÍA','HORA','CURSO','GRUPO','NOMBRE','DOCID','TIME','ASIST','numTicket','linkTicket','linkZoom']
    titles_num=[]
    colum_names=[]
    for i in range(sheet.ncols): 
        colum_names.append(sheet.cell_value(0, i))
    for title in colum_titles:
        if title in colum_names:
            titles_num.append(colum_names.index(title))
        else:
            print('Error: No se encuentra el dato %s'% title)
            break
    colum_titles_num=dict(zip(colum_titles,titles_num))

    all_data=[]
    for j in range(1,sheet.nrows):
        data_dict ={
            'CICLO':return_value_xls(sheet,j,colum_titles_num,'CICLO'),
            'SEMANA':return_value_xls(sheet,j,colum_titles_num,'SEMANA'),
            'PLAT':return_value_xls(sheet,j,colum_titles_num,'PLAT'),
            'DÍA':xlrd.xldate.xldate_as_datetime(return_value_xls(sheet,j,colum_titles_num,'DÍA'),0).strftime('%d/%m/%y'),
            'HORA':return_value_xls(sheet,j,colum_titles_num,'HORA'),
            'CURSO':return_value_xls(sheet,j,colum_titles_num,'CURSO'),
            'GRUPO':return_value_xls(sheet,j,colum_titles_num,'GRUPO'),
            'NOMBRE':return_value_xls(sheet,j,colum_titles_num,'NOMBRE'),
            'DOCID':return_value_xls(sheet,j,colum_titles_num,'DOCID'),
            'TIME':return_value_xls(sheet,j,colum_titles_num,'TIME'),
            'ASIST':return_value_xls(sheet,j,colum_titles_num,'ASIST'),
            'numTicket':return_value_xls(sheet,j,colum_titles_num,'numTicket'),
            'linkTicket':return_value_xls(sheet,j,colum_titles_num,'linkTicket'),
            'linkZoom':return_value_xls(sheet,j,colum_titles_num,'linkZoom')

            }
        all_data.append(data_dict)
    return all_data

def take_somepics(browser=None,all_data=[],file_excel=''):
    if len(all_data)<1:
        all_data = read_data(file_excel=file_excel)
    for data in all_data:
        if data['linkTicket']=='':
            break
        takepic(browser=browser,linkpic=data['linkTicket'],numTicket=data['numTicket'])

def all_report(file_excel='', all_data=[]):
    document = Document()
    if len(all_data)<0:
        all_data = read_data(file_excel=file_excel)
    for i, data in enumerate(all_data):
        if data['linkTicket']=='':
            break
        addTicket(document=document, data=data)
        if i%2!=0:
            document.add_page_break()
    document.save('word//report'+datetime.datetime.today().strftime('%d-%m-%y')+'.docx')
'''
    ss = Screenshot_Clipping.Screenshot()
    driver = webdriver.Chrome(executable_path=driver_path)
    url = "https://www.browserstack.com/"

    driver.get(url)
    image = ss.full_Screenshot(driver, save_path=r'.' , image_name='name.png')

    screen = Image.open(image)
    screen.show()
'''
def all_data_main(op, String):
    idx=op.index(String)
    if len(op) > idx:
        if os.path.isfile(op[idx+1]):
            #file_excel=r'.//excel//Formato_asistencia_tutorias.xls'
            file_excel=op[idx+1]#r'.//excel//Formato_asistencia_tutorias.xls'
            all_data =read_data(file_excel=file_excel)
            return file_excel, all_data
        else:
            print("Error: File don't found.")
    else:
        print("Error: Need path Folder.")
    return None, None

if __name__ == '__main__':
    data_login={
        'user':"miguelnoriega",
        'psswd':'*******'
    } 
    driver_path=r'.//Drivers//chromedriver.exe'

    op = True
    browser=None
    all_data=None
    file_excel=None
    while(op!=False):
        op = input(">> ")
        op = op.split(' ')
        
        if "chromedriver" in op:
            idx=op.index('chromedriver')+1
            if len(op)>=idx: 
                driver_path=op[idx]#r'.//Drivers//chromedriver.exe'
            print('OK.')
            #op=True
        if "data" in op:
            file_excel, all_data = all_data_main(op,'data')
            print('OK.')
            #op=True
        if "take_somepics" in op:
            file_excel, all_data = all_data_main(op,'take_somepics')
            if file_excel != None:
                if browser == None:
                        browser=open_browser(driver_path=driver_path)  
                        browser.maximize_window()
                        openMDA(browser=browser,data_login=data_login)
                        time.sleep(1)  
                take_somepics(browser=browser, file_excel=file_excel)#r'.//excel//Formato_asistencia_tutorias.xls')
                print('Finish')
                #op=True
        if "reports" in op:
            if all_data == None:
                file_excel, all_data = all_data_main(op,'reports')
            all_report(all_data=all_data)
            print('OK.')
            #op=True
        if "exit" in op:
            op=False
        
    '''
    driver_path=r'..//..//..//Drivers//chromedriver.exe'
    browser=open_browser(driver_path=driver_path)  
    browser.maximize_window()
    openMDA(browser=browser)
    time.sleep(1)

    file_excel=r'.//excel//Formato_asistencia_tutorias.xls'
    all_data =read_data(file_excel=file_excel)
    all_report(all_data=all_data)

    #print(all_data[0])
    #linkpic='https://mda.uis.edu.co/scp/tickets.php?id=4095'
    #takepic(browser=browser, linkpic=linkpic, numTicket=556533)
    #take_somepics(browser=browser, file_excel=r'.//excel//Formato_asistencia_tutorias.xls')
    
    os.system('pause')
    print("HI")
        '''
